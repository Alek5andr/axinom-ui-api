# Tech stack
* Node.js
* TypeScript
* WebDriver.io 7
* Chrome
* Mocha
* supertest (superagent)
* Allure

# Authorization token configuration
Set valid token without 'Brearer ' to environment varibale 'TOKEN' in file `.env` between single quotes.

# Running tests
* Install dependencies by executing command `npm install`
  * Replace dependency `chromedriver` version according to version of your Google Chrome browser
* Launch tests by executing command `npm test`

# Image Explorer bugs?
* Bulk checking does not allow to uncheck 1 by 1
* Bulk deletion does not update the page after confirmation. So the images are still visible

# Test assignment
Would be good to specify in the test assignment, that acquiring an authorization token should be programmatically. Manually is fine. 