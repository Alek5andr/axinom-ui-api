import * as request from 'supertest';
import logger from '../../../services/logger';
import logIn from '../post/log.in'

class Authorization {
    axauthDemoUrl: string = 'https://axauth-demo.axservices.net';
    interactionLocation: string = '';
    authenticationCookies: string = '';
    interactionResumeCookie: string = '';

    async getLocationOfInteractionCookieSetting(): Promise<string> {
        const messageString: string = 'ocation of interaction cookie';
        let location: string = '';

        await request('')
            .get('https://id-demo.axservices.net/5ccff80e-4d72-443a-a216-f040af737a56/7acdc869-61c2-4a56-a1e7-975fc6f88434/auth?providerId=AX_AUTH&originUrl=https://vlmrdq.portal-demo.axservices.net/images&idServiceClientUrl=https://id-demo.axservices.net')
            .expect(302)
            .then(res => {
                logger.debug('auth?provider');
                logger.debug(res);
                location = res.headers.location;
            })
            .catch((err: any) => {
                logger.error(` Failed to get l${messageString} setting:`);
                this.loggingError(err);
            });

        logger.info(`L${messageString} is: ${location}`);
        return location;
    }

    private async getInteractionLocationWithCookies(): Promise<void> {
        await request('')
            .get(await this.getLocationOfInteractionCookieSetting())
            .expect(302)
            .then(res => {
                this.authenticationCookies = res.headers['set-cookie'].toString().replace(',_', '; _');
                this.interactionLocation = res.headers.location;

                const matches: RegExpMatchArray | null = this.authenticationCookies.match(/_interaction_resume=.{21}/);
                if (matches) this.interactionResumeCookie = matches[0];
            })
            .catch((err: any) => {
                logger.error(` Failed to get interaction location with cookies:`);
                this.loggingError(err);
            });

        logger.info('Authentication cookies: ' + this.authenticationCookies);
        logger.info('Interaction location: ' + this.interactionLocation);
    }

    private async getAuthenticationLocationWithCookie(): Promise<string> {
        const messageString: string = 'ocation with authentication cookie';
        let location: string = '';

        await this.getInteractionLocationWithCookies();
        await request('')
            .get(this.axauthDemoUrl + this.interactionLocation)
            .set('Cookie', this.authenticationCookies)
            .expect(302)
            .then((res: { headers: {location: string}; })=> {
                location = res.headers.location;
            })
            .catch((err: any) => {
                logger.error(` Failed to get l${messageString}:`);
                this.loggingError(err);
            });;

        logger.info(`L${messageString} is: ${location}`);
        return location;
    }

    async getAuthenticationCallbackLocation(): Promise<string> {
        const messageString: string = 'thentication callback location';
        let location: string = '';

        await request('')
        .get(await this.getAuthenticationLocationWithCookie())
        .set('Cookie', this.authenticationCookies)
        //.redirects()
        //.expect(200)
        .then(res => {
            location = res.headers.location;
            logger.debug('/auth/?');
            logger.debug(res);
            
            this.authenticationCookies = res.headers['set-cookie'].toString().replace(/,_/g, '; _');
        })
        .catch((err: any) => {
            logger.error(` Failed to get a${messageString}:`);
            this.loggingError(err);
        });;

    logger.info('New authentication cookies: ' + this.authenticationCookies);
    logger.info(`A${messageString} is: ${location}`);

    await logIn.logInWithCredentials(this.axauthDemoUrl, this.interactionLocation, this.authenticationCookies);
    return location;
    }

    private loggingError(err: any) {
        const errorString: string = 'Error';
        logger.error(`${errorString} message: ${err.message}`);
        logger.error(`${errorString} response: ${err.response}`);
    }
}

export default new Authorization();