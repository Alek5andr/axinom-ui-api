import * as request from 'supertest';
import logger from "../../../services/logger";

class LogIn {
    async logInWithCredentials(axauthDemoUrl: string, interactionLocation: string, authenticationCookies: string): Promise<void> {
        let authLocation: string = '';

        await request('')
            .post(axauthDemoUrl + interactionLocation + '/login')
            .set('Cookie', authenticationCookies)
            .send('login=' + process.env.AXINOM_USER)
            .send('password=' + process.env.AXINOM_USER)
            .then(res => {
                logger.debug('after login');
                logger.debug(res);
                logger.debug('after headers');
                logger.debug(res.headers);
                authLocation = res.headers.location;
            });

        logger.debug('authLocation = ' + authLocation);
        let confirmLocation: string = '';
        await request('')
            .get(authLocation)
            .set('Cookie', authenticationCookies)
            .redirects()
            .expect(200)
            .then(res => {
                confirmLocation = res
                logger.debug('confirmLocation?');
                logger.debug(confirmLocation);
            })

        let newLocation: string = '';
        await request('')
            .post(axauthDemoUrl + interactionLocation + '/confirm')
            .set('Cookie', authenticationCookies)
            //.redirects()
            .then(res => {
                logger.debug('confirm?')
                logger.debug(res);
                newLocation = res.headers.location;
            });
        
        await request('')
            .get(newLocation)
            .set('Cookie', authenticationCookies)
            .then(res => {
                logger.debug(res);
            });
    }
}

export default new LogIn();