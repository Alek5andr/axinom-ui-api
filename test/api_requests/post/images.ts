import request from '../request';
import logger from '../../services/logger';

class Images {
    async deleteBulkImagesByTitle(imageTitles: string[]): Promise<boolean> {
        const ids: string[] = await this.getArrayOfFilteredImageIds(imageTitles);
        const graphQlQuery: string = '{deleteImages(filter: $filter) {affectedIds __typename}}';
        const requestBody: Object = {
            "query": `mutation BulkDeleteImages($filter: ImageFilter) ${graphQlQuery}`,
            "variables": {"filter":{"id":{"in":ids}}},
            "operationName": "BulkDeleteImages"
          };
        const res: any = await request.post(requestBody);
        const body: any = res.body;
        logger.info(' Bulk image deletion response body: ' + JSON.stringify(body));

        return await this.areImagesByIdDeleted(body.data.deleteImages.affectedIds);
    }

    private async getImageNodes(properties = 'id title'): Promise<any> {
        const graphQlQuery: string = `{images {nodes {${properties} }}}`;
        const requestBody = {
            "query": `query GetImageNodes ${graphQlQuery}`,
            "variables": null,
            "operationName": "GetImageNodes"
          };
        const res: any = await request.post(requestBody);
        const imageNodes: any = res.body.data.images.nodes;
        logger.info(' Image nodes: ' + JSON.stringify(imageNodes));
        return imageNodes;
    }

    private async getArrayOfFilteredImageIds(imageTitles: string[]): Promise<string[]> {
        const nodes: any = await this.getImageNodes();
        let arrayOfimageIds: string[] = [];

        for (const imageTitle of imageTitles) {
            for (const node of nodes) {
                if(node.title === imageTitle) {
                    arrayOfimageIds.push(node.id);
                }
            }
        }

        logger.info('Array of filtered image ids: ' + arrayOfimageIds);
        return arrayOfimageIds;
    }

    private async areImagesByIdDeleted(affectedIds: string[]): Promise<boolean> {
        const imageNodes: any = await this.getImageNodes();
        for (const imageNode of imageNodes) {
            if (affectedIds.includes(imageNode)) {
                logger.debug(imageNode);
                return false;
            }
        }
        return true;
    }
}

export default new Images();