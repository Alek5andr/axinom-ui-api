import * as dotenv from 'dotenv';
import * as supertest from 'supertest';
import logger from '../services/logger';

class Request {
    constructor() {
        dotenv.config();
    }

    async post(requestBody: Object): Promise<any> {
        logger.info('Sending POST request with body: ' + JSON.stringify(requestBody));

        const res: any = await supertest('')
        .post(process.env.API_URL)
        .type('application/json')
        .auth(process.env.TOKEN, {type: 'bearer'})
        .send(requestBody)
        .expect(200);
        
        const errorMessage: any = res.body.errors;
        if (errorMessage !== undefined) throw new Error(JSON.stringify(res.body.errors));
        return res;
    }
}

export default new Request();