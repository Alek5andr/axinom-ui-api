import { ILogObject, Logger } from "tslog";
import { writeFileSync }  from "fs";

function logDebugToFile(logObject: ILogObject) {
    logToTransport("./logs/debug.log", logObject);
}

function logInfoToFile(logObject: ILogObject) {
    logToTransport("./logs/info.log", logObject);
}

function logErrorToFile(logObject: ILogObject) {
    logToTransport("./logs/error.log", logObject);
}

function logOthersToFile(logObject: ILogObject) {
    logToTransport("./logs/others.log", logObject);
}

function logToTransport(fileName: string, logObject: ILogObject) {
    writeFileSync(fileName,
        JSON.stringify(logObject.date) + ":" +
        JSON.stringify(logObject.logLevel) + ":" +
        JSON.stringify(logObject.fileName) + ":" +
        JSON.stringify(logObject.lineNumber) + ":" +
        JSON.stringify(logObject.columnNumber) + ":" +
        JSON.stringify(logObject.argumentsArray) + "\n",
        { encoding: "utf8", flag: "a" }
    );
}

const logger: Logger = new Logger();
logger.attachTransport(
  {
    silly: logOthersToFile,
    debug: logDebugToFile,
    trace: logOthersToFile,
    info: logInfoToFile,
    warn: logOthersToFile,
    error: logErrorToFile,
    fatal: logOthersToFile,
  },
  "debug"
);

export default logger;

logger.debug("I am a debug log.");
logger.info("I am an info log.");
logger.warn("I am a warn log with a json object:", { foo: "bar" });