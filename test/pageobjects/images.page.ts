import logger from "../services/logger";
import Page from "./page";
import keyboard from '../pageobjects/keyboard';

class Images extends Page {
    // Selectors
    get columnArrowsSelector() { return 'div[class^="ColumnLabel_arrows_"]>svg>path'}

    // Elements
    get uploadButton() { return $('') }
    get sortableColumns() { return $$('div[class*="ColumnLabel_sortable"]') }
    get imageTitles() { return $$('div[class^="ListRow_row_"] p') }
    get filterTitles() { return $$('div[class^="Filter_container_"]') }

    clickUploadButton(): string {
        super.clickButtonWithText('UPLOAD');
        return super.getCurrentUrl();
    }

    sortByColumnLabelInSpecificOrder(label: string, order: string): boolean {
        const arrows: WebdriverIO.Element[] = this.clickOnColumnLabel(label).$$(this.columnArrowsSelector);
        
        switch (order) {
            case "ascending":
                return this.isSorted(arrows[0]);
            case "descending":
                return this.isSorted(arrows[1]);
            default:
                throw new Error('No such order is implemented: ' + order);
        }
    }

    private isSorted(arrow: WebdriverIO.Element): boolean {
        return arrow.getAttribute('class').includes('ColumnLabel_sorted_');
    }

    areImagesSortedInSpecificOrder(image1Title: string, image2Title: string, order: string) {
        const map: Map<string, number> = this.getImagesPositions(image1Title, image2Title)
        let image1Position: number = 0;
        let image2Position: number = 0;

        let position = map.get(image1Title)
        image1Position = position != undefined ? position : 0;

        position = map.get(image2Title)
        image2Position = position != undefined ? position : 0;
        
        switch (order) {
            case "ascending":
                return image1Position < image2Position;
            case "descending":
                return image1Position > image2Position;
            default:
                throw new Error('No such order is implemented: ' + order);
        }
    }

    private getImagesPositions(image1Title: string, image2Title: string): Map<string, number> {
        let imagesPositons: Map<string, number> = new Map();
        let array: string[] = [];

        this.imageTitles.forEach(imageTitle => array.push(imageTitle.getText()));
        array.forEach(title => {
            if (image1Title === title) imagesPositons.set(title, array.indexOf(image1Title))
            if (image2Title === title) imagesPositons.set(title, array.indexOf(image2Title))
        });

        return imagesPositons
    }

    clickOnColumnLabel(label: string): WebdriverIO.Element {
        const element: WebdriverIO.Element = super.findElementByLabel(this.sortableColumns, label);
        super.clickElement(element);
        return element;
    }
    
    filterImagesByImageParameter(filterLabel: string, filterValue: string): boolean {
        switch (filterLabel) {
            case 'Title':
                return this.setFilterValueByFilterLabel(filterLabel, filterValue);
            default:
                throw new Error('No such filter title is implemented yet: ' + filterLabel);
        }
    }

    private setFilterValue(filterLabel: string, filterValue: string) {
        logger.info(`Setting value "${filterValue}" to filter "${filterLabel}"`);
        super.clickElement(super.findElementByLabel(this.filterTitles, filterLabel));
        super.setValueToInputFieldOfElementFoundByLabel(this.filterTitles, filterValue, filterLabel);
        keyboard.pressEnter();
        super.sleep();
    }

    private isFilteredByImageTitle(imageTitle: string): boolean {
        logger.info(`Is image list filtered by image title "${imageTitle}"?`);
        for (const element of this.imageTitles) {
            const regex: RegExp = new RegExp(imageTitle, 'si');
            if (element.getText().match(regex) === null) return false;
        }
        return true;
    }

    private setFilterValueByFilterLabel(filterLabel: string, filterValue: string): boolean {
        this.setFilterValue(filterLabel, filterValue);
        return this.isFilteredByImageTitle(filterValue);
    }
}

export default new Images();