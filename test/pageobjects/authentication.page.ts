import logger from "../services/logger";
import Page from "./page";

class AuthenticationPage extends Page {
    get authenticationButton() { return $('div[class*="Login-module_tile_"]') }

    authenticate(): string {
        logger.info('Authenticating');
        super.clickElement(this.authenticationButton);
        return super.getPageTitle();
    }
}

export default new AuthenticationPage();