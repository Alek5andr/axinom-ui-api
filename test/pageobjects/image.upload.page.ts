import logger from "../services/logger";
import ImageDetails from "./image.details.page";

class ImageUpload extends ImageDetails {
    get imageSource() { return $('#file') }
    get imageTypeList() { return $('select[name="imageType"]') }
    get proceedButton() { return $('div[class^="Action_container_"]') }

    private setImagePath(path: string) {
        logger.info('Setting image path: ' + path);
        this.imageSource.addValue(path);
    }

    selectImageType(type: string): string {
        logger.info('Selecting image type: ' + type);
        this.imageTypeList.selectByAttribute('value', type);
        return this.imageTypeList.getValue();
    }

    uploadImage(imagePath: string): string {
        this.setImagePath(imagePath);
        super.clickElement(this.proceedButton);
        return super.getCurrentUrl();
    }
}

export default new ImageUpload();