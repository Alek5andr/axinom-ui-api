class Keyboard {
    clearText(element: any) {
        (element ?? browser).keys(['Control', 'a', 'Control', 'Delete']);
    }

    pressEnter() {
        browser.keys('Enter');
    }
}

export default new Keyboard();