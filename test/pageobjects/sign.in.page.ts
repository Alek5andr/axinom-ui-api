import logger from "../services/logger";
import Page from "./page";

class SignIn extends Page {
    get emailAddressInput() { return $('[name=login]') }
    get passwordInput() { return $('[name=password]') }
    
    private logIn (email: string, password: string) {
        logger.info(`Singing in with credentials: ${email}:${password}`);
        this.emailAddressInput.setValue(email);
        this.passwordInput.setValue(password);
        super.clickButtonWithText('Sign-in');
    }

    private authorize(): string {
        logger.info('Authorizing');
        super.clickButtonWithText('Continue');
        super.sleep(3000);
        return super.getPageTitle();
    }

    logInAndAuthorize(email, password): string {
        this.logIn(email, password);
        return this.authorize();
    }
}

export default new SignIn();