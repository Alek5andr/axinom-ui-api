/**
* main page object containing all methods, selectors and functionality
* that is shared across all page objects
*/
import logger from "../services/logger";
import keyboard from '../pageobjects/keyboard';

export default class Page {
    private errorMessage: string = 'lement is not found with such label: ';

    // DOM shared tags
    private anchor: string = 'a';
    private input: string = 'input';
    private button: string = 'button';

    getPageTitle(): string {
        const title: string = browser.getTitle();
        logger.info('Returning page title: ' + title);
        return title;
    }
   
    clickElement(element: WebdriverIO.Element): boolean {
        const isExisting = element.isExisting();
        const elementsText = element.getText();
        
        logger.info(' Clicking on element with selector: ' + element.selector);

        if (isExisting) {
            element.scrollIntoView();
            element.click();
            this.sleep();
            logger.debug('  Element (with text) clicked: ' + elementsText);
        }
        
        return isExisting;
    }

    clickElementBySelector(selector: string): boolean {
        return this.clickElement($(selector));
    }

    clickButtonWithText(text: string): boolean {
        return this.clickElementBySelector(`${this.button}=${text}`);
    }

    getCurrentUrl(): string {
        const currentUrl: string = browser.getUrl();
        logger.info('Returning current url: ' + currentUrl);
        return currentUrl;
    }

    sleep(ms: number = 1250) {
        browser.pause(ms);
    }

    clickAnchorContainingText(text: string): boolean {
        const element = $(`${this.anchor}*=${text}`);
        return this.clickElement(element);
    }

    navigateToImagesPage(): string {
        const pageName: string = 'Images';
        logger.info('Navigating to page: ' + pageName);
        this.clickAnchorContainingText(pageName);
        this.sleep();
        return this.getCurrentUrl();
    }

    findElementByLabel(elements: Array<WebdriverIO.Element>, label: string): WebdriverIO.Element {
        for (const element of elements) {
            logger.debug(`Iterating elements with text: ${label} = ${element.getText()}?`);

            if (element.getText().includes(label)) {
                logger.debug(` Returning found by label "${label}" element. Its selector is "${element.selector}"`);
                return element;
            }
        }
        throw new Error(`E${this.errorMessage}${label}`);
    }

    setValueToInputFieldByParentElementLabel(elements: Array<WebdriverIO.Element>, value: string, label: string): boolean {
        return this.setValueToInputFieldOfElement(this.getParentElementByLabel, elements, value, label);
    }

    setValueToInputFieldOfElementFoundByLabel(elements: Array<WebdriverIO.Element>, value: string, label: string): boolean {
        return this.setValueToInputFieldOfElement(this.findElementByLabel, elements, value, label);
    }

    private setValueToInputFieldOfElement (fn: (elements: Array<WebdriverIO.Element>, text: string) => WebdriverIO.Element, elements: Array<WebdriverIO.Element>, value: string, label: string): boolean {
        let element: any = null;

        if (label != null) {
            logger.info(`Setting value "${value}" to input field of element found by label "${label}"`);
            return this.clearInputElementAndSetValue(fn(elements, label).$(this.input), value);
        }

        logger.info(`Setting value "${value}" to input field`);
        return this.clearInputElementAndSetValue($(this.input), value);
    }

    private clearInputElementAndSetValue(element: WebdriverIO.Element, value: string): boolean {
        const isExisting = element.isExisting();
        if(isExisting) {
            this.clearInputValue(element);
            element.setValue(value);
        }
        return isExisting;
    }

    getParentElementByLabel(elements: Array<WebdriverIO.Element>, label: string): WebdriverIO.Element {
        for (const element of elements) {
            logger.debug(`Iterating elements with text: ${label} = ${element.getText()}?`);

            if (element.getText().includes(label)) {
                const parentElement: WebdriverIO.Element = element.parentElement();
                logger.debug(` Returning found by label "${label}" element's parent element: ${Object.entries(parentElement.parent).find(node => node.includes('selector'))}`);
                return parentElement;
            }
        }
        throw new Error(`Parent e${this.errorMessage}${label}`);
    }

    clearInputValue(element: WebdriverIO.Element): boolean {
        let isCleaared: boolean = false;
        if (this.clickElement(element)) {
            keyboard.clearText(element);
            isCleaared = true;
        }
        return isCleaared;
    }
}
