import Page from "./page";

export default class ImageDetails extends Page {
    private get imageTitleLabels() {return $$('div>label') }

    setImageTitle(imageTitle: string): boolean {
        return this.setValueToInputFieldByTitleLabel(imageTitle, 'Title');
    }

    setValueToInputFieldByTitleLabel(value: string, label: string): boolean {
        return super.setValueToInputFieldByParentElementLabel(this.imageTitleLabels, value, label);
    }
}