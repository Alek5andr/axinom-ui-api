import authenticationPage from '../pageobjects/authentication.page';
import signInPage from '../pageobjects/sign.in.page';
import imagesPage from '../pageobjects/images.page';
import imageUploadPage from '../pageobjects/image.upload.page';
import * as path from 'path';
import images from '../api_requests/post/images';
import logger from '../services/logger';

describe('My Login application', () => {
    const imageATitle: string = 'Image A';
    const imageXTitle: string = 'Image X';
    const titleLabel: string = 'Title';
    const sortingOrder: string = 'ascending';

    it('Should authenticate', () => {
        expect(authenticationPage.authenticate()).toEqual('Sign-in');
    });

    it('Should log in with valid credentials and authorize', () => {
        expect(signInPage.logInAndAuthorize(process.env.AXINOM_USER, process.env.AXINOM_PASS)).toEqual('Axinom Mosaic - Management System');
    });

    it('Uploading images', () => {
        const imageAPath: string = '../resources/image_a.jpg';
        const imageXPath: string = '../resources/image_x.jpg';
        const imageType: string = 'movie_cover';

        uploadImageAndVerify(imageType, imageAPath, imageATitle);
        uploadImageAndVerify(imageType, imageXPath, imageXTitle);
    });

    it(`Sorting column with label "${titleLabel}" in "${sortingOrder}" order`, () => {
        expect(imagesPage.sortByColumnLabelInSpecificOrder(titleLabel, sortingOrder)).toBe(true);
        expect(imagesPage.areImagesSortedInSpecificOrder(imageATitle, imageXTitle, sortingOrder)).toBe(true);
    });

    it(`Filtering images by "${titleLabel}" with value "${imageATitle}"`, () => {
        expect(imagesPage.filterImagesByImageParameter(titleLabel, imageATitle)).toBe(true);
    });

    it('Deleting bulk images', async () => {//browser.debug();
        expect(await images.deleteBulkImagesByTitle([imageATitle, imageXTitle])).toBe(true);
    });

    function uploadImageAndVerify(imageType: string, imagePath: string, imageTitle: string) {
        expect(imagesPage.clickUploadButton()).toContain('/upload');
        expect(imageUploadPage.selectImageType(imageType)).toEqual(imageType);
        expect(imageUploadPage.uploadImage(path.join(__dirname, imagePath))).toMatch(/images\/.*?-.*?-.*?-.*?-.+/);
        expect(imageUploadPage.setImageTitle(imageTitle)).toBe(true);
        expect(imagesPage.navigateToImagesPage()).toMatch(/images$/);
    }
});


